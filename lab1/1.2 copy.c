//
//  main.c
//  1.1
//
//  Created by Vladii Egorov on 2014-12-15.
//  Copyright (c) 2014 Vladii Egorov. All rights reserved.
//

#include <stdio.h>

int main()
{
    int h,m,s;
    
    printf("what time is it? HH:MM:SS \n");
    scanf( "%d %d %d",&h,&m,&s);
    
    if ((h == 12) && ( m == 0))
        printf("good day \n");
    else if ((h>12) && (h<17))
        printf("good day \n");
    else if (h<12)
        printf("good morning \n");
    else if ((h>=17) && (h<= 20))
        printf("good evening \n");
    else if (h>20)
        printf("good night \n");
    
    return 0;
}

