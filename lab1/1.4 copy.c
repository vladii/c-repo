//
//  main.c
//  1.4
//
//  Created by Vladii Egorov on 19/01/15.
//  Copyright (c) 2015 Vladii Egorov. All rights reserved.
//

#include <stdio.h>

int main()
{
    float feet,inches;
    float m;
    
    printf("Enter what you want to convert \n");
    scanf (" %f %f",&feet,&inches);
    
    inches /=12;
    feet +=inches;
    m =feet/3.25;
    printf("In meters that would be: %.1f \n",m);
    
    return 0;
}

